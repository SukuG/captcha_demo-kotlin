package com.captcha.demo.model

interface IMainActivityPresenter {

    fun onLoadCaptcha()
}