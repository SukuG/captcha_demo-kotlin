package com.captcha.demo.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.captcha.demo.R
import com.captcha.demo.databinding.ActivityMainBinding
import com.captcha.demo.model.IMainActivityPresenter
import com.captcha.demo.model.IMainActivityView
import com.captcha.demo.presenter.MainActivityPresenter

class MainActivity : AppCompatActivity(), IMainActivityView {

    private var mainActivityPresenter: IMainActivityPresenter? = null
    private var dataBinding: ActivityMainBinding? = null
    private var originalCaptchaValue: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //dataBinding?.root
        init()
        initListener()
    }

    private fun init() {
        mainActivityPresenter = MainActivityPresenter(this)
        mainActivityPresenter?.onLoadCaptcha()
    }

    private fun initListener() {
        dataBinding?.btRefreshCaptcha?.setOnClickListener {
            mainActivityPresenter?.onLoadCaptcha()
        }
        dataBinding?.btValidateCaptcha?.setOnClickListener {
            validation(dataBinding?.etTypeCaptcha?.text.toString())
        }
    }

    override fun setCaptchaText(toString: String) {
        originalCaptchaValue = toString
        dataBinding?.tvCaptchaText?.text = toString
    }

    private fun validation(value: String) {
        if (value == "" || value.isEmpty())
            Toast.makeText(this, "Type Captcha Text", Toast.LENGTH_LONG).show()

        if (value == originalCaptchaValue)
            Toast.makeText(this, "Validation is Success", Toast.LENGTH_LONG).show()
        else
            Toast.makeText(this, "Validation is Failure", Toast.LENGTH_LONG).show()
    }
}
