package com.captcha.demo.presenter

import com.captcha.demo.model.IMainActivityPresenter
import com.captcha.demo.model.IMainActivityView
import java.util.*


class MainActivityPresenter(var mainActivityView: IMainActivityView) : IMainActivityPresenter {

    private var array = charArrayOf(
        '@', '#', '$', '%', '&', '?',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z',
        '0', '1', '2', '3', '4', '5',
        '6', '7', '8', '9',
        'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x',
        'y', 'z'
    )

    override fun onLoadCaptcha() {
        val finalShuffledValues = StringBuffer()
        val solutionArray = shuffleArray()
        val fiveElements = solutionArray.take(5)
        for (element in fiveElements) {
            finalShuffledValues.append(element)
        }
        mainActivityView.setCaptchaText(finalShuffledValues.toString())
    }

    // Implementing Fisher–Yates shuffle
    private fun shuffleArray(): CharArray {
        val random = Random()
        for (i in array.size - 1 downTo 1) {
            val index: Int = random.nextInt(i + 1)
            val a = array[index]
            array[index] = array[i]
            array[i] = a
        }
        return array
    }
}